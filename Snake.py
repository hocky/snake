import sys
import math
import pygame
import random
import tkinter as tk
from tkinter import messagebox

pygame.init()
pygame.display.set_caption("Ular!")

maxFood = 4
w = 20
h = 20
spawnX = w//2
spawnY = h//2
sz = 32
dx = [-1,0,1,0]
dy = [0,1,0,-1]
win = pygame.display.set_mode((500,500))
clock = pygame.time.Clock()

# Background load
Background_Image = [pygame.image.load('./data/image/background/soil.png').convert(),pygame.image.load('./data/image/background/grass.png').convert(),pygame.image.load('./data/image/background/sand.png').convert(),pygame.image.load('./data/image/background/snow.png').convert(),pygame.image.load('./data/image/background/menu.png').convert(),pygame.image.load('./data/image/background/pause.png').convert_alpha()]

# Barrier load
Barrier_Image = [pygame.image.load('./data/image/items/Stone.png').convert_alpha()]

# Food load
Food_Image = [pygame.image.load('./data/image/items/Banana.png').convert_alpha(), pygame.image.load('./data/image/items/Apple.png').convert_alpha(), pygame.image.load('./data/image/items/Pineapple.png').convert_alpha(), pygame.image.load('./data/image/items/Grapes.png').convert_alpha(),pygame.image.load('./data/image/items/Water.png').convert_alpha()]

# Buttons
Button_Image = [pygame.image.load('./data/image/buttons/play.png').convert_alpha(), pygame.image.load('./data/image/buttons/play_highlighted.png').convert_alpha(),pygame.image.load('./data/image/buttons/playCircle.png').convert_alpha(),pygame.image.load('./data/image/buttons/playCircle2.png').convert_alpha(),pygame.image.load('./data/image/buttons/quit.png').convert_alpha(),pygame.image.load('./data/image/buttons/quit_highlighted.png').convert_alpha(),pygame.image.load('./data/image/buttons/quitCircle.png').convert_alpha(),pygame.image.load('./data/image/buttons/quitCircle2.png').convert_alpha()]

# Snake load
Snake_Image = [
[pygame.image.load('./data/image/theSnake/01.png').convert_alpha(),pygame.image.load('./data/image/theSnake/01.png').convert_alpha(),pygame.image.load('./data/image/theSnake/02.png').convert_alpha(),pygame.image.load('./data/image/theSnake/03.png').convert_alpha(),pygame.image.load('./data/image/theSnake/04.png').convert_alpha()],
[pygame.image.load('./data/image/theSnake/10.png').convert_alpha(),pygame.image.load('./data/image/theSnake/10.png').convert_alpha(),pygame.image.load('./data/image/theSnake/12.png').convert_alpha(),pygame.image.load('./data/image/theSnake/13.png').convert_alpha(),pygame.image.load('./data/image/theSnake/14.png').convert_alpha()],
[pygame.image.load('./data/image/theSnake/20.png').convert_alpha(),pygame.image.load('./data/image/theSnake/21.png').convert_alpha(),pygame.image.load('./data/image/theSnake/21.png').convert_alpha(),pygame.image.load('./data/image/theSnake/23.png').convert_alpha(),pygame.image.load('./data/image/theSnake/24.png').convert_alpha()],
[pygame.image.load('./data/image/theSnake/30.png').convert_alpha(),pygame.image.load('./data/image/theSnake/31.png').convert_alpha(),pygame.image.load('./data/image/theSnake/32.png').convert_alpha(),pygame.image.load('./data/image/theSnake/32.png').convert_alpha(),pygame.image.load('./data/image/theSnake/34.png').convert_alpha()],
[pygame.image.load('./data/image/theSnake/40.png').convert_alpha(),pygame.image.load('./data/image/theSnake/41.png').convert_alpha(),pygame.image.load('./data/image/theSnake/42.png').convert_alpha(),pygame.image.load('./data/image/theSnake/43.png').convert_alpha(),pygame.image.load('./data/image/theSnake/43.png').convert_alpha()]
]

# Sound effects
sound_effects = [pygame.mixer.Sound('./data/sound/effects/1.wav'),pygame.mixer.Sound('./data/sound/effects/2.wav'),pygame.mixer.Sound('./data/sound/effects/3.wav'),pygame.mixer.Sound('./data/sound/effects/4.wav'),pygame.mixer.Sound('./data/sound/effects/5.wav'),pygame.mixer.Sound('./data/sound/effects/6.wav')]

class barrier(object):

    def __init__(self,start,item_type):
        self.pos = start
        self.item_type = item_type

    def move(self,nx,ny):
        self.pos = (nx,ny)

    def draw(self,surface):
        global sz
        i = self.pos[0]
        j = self.pos[1]
        surface.blit(Barrier_Image[self.item_type],(i*sz,j*sz))


class food(object):
    def __init__(self,start,item_type):
        self.pos = start
        self.item_type = item_type

    def move(self,nx,ny):
        self.pos = (nx,ny)

    def draw(self,surface):
        global sz
        i = self.pos[0]
        j = self.pos[1]
        surface.blit(Food_Image[self.item_type],(i*sz,j*sz))

class snake_part(object):
    def __init__(self,start):
        self.pos = start
    
    def move(self,nx,ny):
        self.pos = (nx,ny)

    def draw(self,surface,snake_from,snake_to):
        global sz
        i = self.pos[0]
        j = self.pos[1]
        surface.blit(Snake_Image[snake_from][snake_to],(i*sz,j*sz))

def get_across(direction):
	direction += 2
	while(direction >= 4): direction -= 4
	return direction

class snake(object):
    # contains bunch of snake_parts, make a list of snake_part
    body = []
    from_where = []
    to_where = []
    # 4 = nowhere
    # Snake contains 2 variable, lists of the body, and the direction of its head
    def __init__(self,pos):
        self.body.append(snake_part(start = pos))
        self.from_where.append(4)
        self.to_where.append(4)
        self.body.append(snake_part(start = pos))
        self.from_where.append(4)
        self.to_where.append(4)
        self.direction = random.randint(0,3)

    def move(self):
        global h,w

        for event in pygame.event.get():
            if(event.type == pygame.QUIT):
                pygame.quit()

        keys = pygame.key.get_pressed()
        if(self.direction == 1 or self.direction == 3):
            if(keys[pygame.K_LEFT]): self.direction = 0
            elif(keys[pygame.K_RIGHT]): self.direction = 2
        elif(self.direction == 0 or self.direction == 2):
            if(keys[pygame.K_DOWN]): self.direction = 1
            elif(keys[pygame.K_UP]): self.direction = 3

        for i, c in enumerate(self.body):  # Loop through every cube in our body
            if(i == 0):
                # Its the head
                nx = c.pos[0]+dx[self.direction]+w
                ny = c.pos[1]+dy[self.direction]+h
                while(nx >= w): nx -= w
                while(ny >= h): ny -= h
                pre = c.pos[:]
                self.from_where[i] = get_across(self.direction)
                self.to_where[i] = 4;
                c.move(nx,ny)
            elif(i == len(self.body)-1):
            	# Its the tail
                nx = pre[0]
                ny = pre[1]

                self.to_where[i] = get_across(self.from_where[i-1])
                self.from_where[i] = 4

                c.move(nx,ny)
            else:
            	# Its the body
            	nx = pre[0]
            	ny = pre[1]
            	pre = c.pos[:]

            	self.from_where[i] = get_across(self.to_where[i])
            	self.to_where[i] = get_across(self.from_where[i-1])

            	c.move(nx,ny)



    def reset(self,pos):
        self.body = []
        self.from_where = []
        self.to_where = []
        self.body.append(snake_part(start = pos))
        self.from_where.append(4)
        self.to_where.append(4)
        self.body.append(snake_part(start = pos))
        self.from_where.append(4)
        self.to_where.append(4)
        self.direction = random.randint(0,3)

    def addCube(self):
        tail = self.body[-1]
        self.body.append(snake_part((tail.pos[0],tail.pos[1])))
        self.from_where.append(4)
        self.to_where.append(4)


    def draw(self,surface):
        global h,w
        for i, c in enumerate(self.body):
            c.draw(surface,snake_from = self.from_where[i],snake_to = self.to_where[i])

def redrawWindowPlay(surface,isWin = 0):
    global s,snack
    surface.blit(Background_Image[0],(0,0))
    if(not isWin): snack.draw(surface)
    s.draw(surface)
    pygame.display.update()

def redrawWindowMenu(surface,isOver = 0,isHold = 0):
    surface.blit(Background_Image[4],(0,0))
    if(isHold == 1): surface.blit(Button_Image[3],(20,280))
    else: surface.blit(Button_Image[2],(20,280))
    if(isOver == 1 or isHold == 1): surface.blit(Button_Image[1],(0,0))
    else: surface.blit(Button_Image[0],(0,0))

    if(isHold == 2): surface.blit(Button_Image[7],(250,350))
    else: surface.blit(Button_Image[6],(250,350))
    if(isOver == 2 or isHold == 2): surface.blit(Button_Image[5],(0,0))
    else: surface.blit(Button_Image[4],(0,0))
    pygame.display.update()

def redrawWindowPause(surface):
    # 290 170
    curw = (w*sz)/2
    curh = (h*sz)/2
    surface.blit(Background_Image[5],(curw-145,curh-85))
    pygame.display.update()

def CrashCheck():
    if(s.body[0].pos in list(map(lambda z:z.pos,s.body[1:]))):
        pygame.mixer.Sound.play(sound_effects[random.randint(4,5)])
        message_box("Permainan Berakhir!","Skor kamu " + str(len(s.body)-1) + "\nMain lagi?")
        s.reset((spawnX,spawnY))
        snack = food(randomSnack(s),random.randint(0,maxFood))
        return 1
    return 0

def randomSnack(item):
    global w,h
    positions = item.body
    able = []
    available = [[0]*h for _ in range(w)]
    for i,c in enumerate(positions):
        available[c.pos[0]][c.pos[1]] = 1
    for i in range(w):
        for j in range(h):
            if(not available[i][j]):
                able.append((i,j))
    index = random.randint(0,len(able)-1)
    return able[index]

def message_box(subject,content):
    root = tk.Tk()
    root.attributes("-topmost",True)
    root.withdraw()
    messagebox.showinfo(subject,content)
    try: root.destroy()
    except: pass

def level_chooser():
    pass

def pause_menu():
    global win

    while 1:
        for event in pygame.event.get():
            if(event.type == pygame.QUIT):
                pygame.quit()
                sys.exit()
        keys = pygame.key.get_pressed()
        if(keys[pygame.K_m]): return 1
        elif(keys[pygame.K_p]): return 0
        redrawWindowPause(win)

def play():
    global w,h,sz,s,snack,win
    win = pygame.display.set_mode((w*sz,h*sz))
    s.reset((spawnX,spawnY))
    snack = food(randomSnack(s),random.randint(0,maxFood))
    while 1:
        # not that fast
        for event in pygame.event.get():
            if(event.type == pygame.QUIT):
                pygame.quit()

        keys = pygame.key.get_pressed()
        if(keys[pygame.K_ESCAPE]):
            if(pause_menu()): return
        clock.tick(10)
        # FPS
        s.move()
        if(s.body[0].pos == snack.pos):
            redrawWindowPlay(win)
            pygame.time.wait(100)
            if(snack.item_type == 4): pygame.mixer.Sound.play(sound_effects[3])
            else: pygame.mixer.Sound.play(sound_effects[random.randint(0,2)])
            s.addCube()
            s.move()
            if(len(s.body) == w*h):
                redrawWindowPlay(win,isWin = 1)
                message_box("Permainan Berakhir!","Kamu berhasil memenuhi ladangnya!\nSelamat!\nMain lagi?")
                s.reset((spawnX,spawnY))
                snack = food(randomSnack(s), random.randint(0,maxFood))
            else:
                snack = food(randomSnack(s), random.randint(0,maxFood))
        redrawWindowPlay(win)
        if(CrashCheck()): continue

def getDistance(posA, posB):
    A = posA[0]-posB[0]
    B = posA[1]-posB[1]
    return A*A+B*B

def isInButton(pos):
    buttonRadius = 48**2
    greenP = (70,330)
    redP = (300,400)
    if(getDistance(greenP,pos) <= buttonRadius): return 1
    if(getDistance(redP,pos) <= buttonRadius): return 2

def main_menu():
    global win
    while 1:
        for event in pygame.event.get():
            if(event.type == pygame.QUIT):
                pygame.quit()
                sys.exit()
            pos = pygame.mouse.get_pos()
            isOver = 0
            isHold = 0
            if(event.type == pygame.MOUSEBUTTONDOWN):
                isHold = isInButton(pos)
            if(event.type == pygame.MOUSEMOTION):
                isOver = isInButton(pos)
        redrawWindowMenu(win,isOver,isHold)
        if(isHold == 1):
            pygame.time.wait(600)
            play()
            isOver = 0
            isHold = 0
            win = pygame.display.set_mode((500,500))
        if(isHold == 2):
            pygame.quit()
            sys.exit()

s = snake((spawnX,spawnY))
main_menu()
play()